/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "usb_device.h"

/* USER CODE BEGIN Includes */
#include "usbd_cdc_if.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void CheckSTM32wMPU9150byI2C(void);
void CheckSTM32wPCbyUSB(void);
void ConfigAccelMPU(void);
void ConfigGyroMPU(void);
void ConfigMagnMPU(void);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
uint16_t slaveAddress = 0xD0; // Address of MPU9150
float mpu9150AS = 0x00; // 
float mpu9150GS = 0x00; // 
/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
	
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USB_DEVICE_Init();

  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  CheckSTM32wMPU9150byI2C(); // Checking work of the STM with MPU by I2C
	CheckSTM32wPCbyUSB(); // Checking work of the STM with PC by USB
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_RESET);
	ConfigAccelMPU();
	ConfigGyroMPU();
	ConfigMagnMPU();
	uint8_t requestToMPU[2] = {0x00, 0x00};
	while (1)
  {

		uint8_t data[22];
		while(HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) {;} // Waiting for the bus I2C to be ready;
		requestToMPU[0] = 0x43;
		data[0] = 0x24;
		data[1] = 0x32;
		while(HAL_I2C_Master_Transmit(&hi2c1, slaveAddress, (uint8_t*)requestToMPU, 1, 1000) != HAL_OK);
		while(HAL_I2C_Master_Receive(&hi2c1, slaveAddress, &data[2], 6, 1000) != HAL_OK);
		// Gyroscope
		uint8_t temp;
		temp = data[3];
		data[3] = data[2];
		data[2] = temp;
		temp = data[5];
		data[5] = data[4];
		data[4] = temp;
		temp = data[7];
		data[7] = data[6];
		data[6] = temp;
	  // Accelerometr
		requestToMPU[0] = 0x3B;
		while(HAL_I2C_Master_Transmit(&hi2c1, slaveAddress, (uint8_t*)requestToMPU, 1, 1000) != HAL_OK);
		while(HAL_I2C_Master_Receive(&hi2c1, slaveAddress, &data[8], 6, 1000) != HAL_OK);
		temp = data[9];
		data[9] = data[8];
		data[8] = temp;
		temp = data[11];
		data[11] = data[10];
		data[10] = temp;
		temp = data[13];
		data[13] = data[12];
		data[12] = temp;
		// Magnetometr
		requestToMPU[0] = 0x03;
		while(HAL_I2C_Master_Transmit(&hi2c1, slaveAddress, (uint8_t*)requestToMPU, 1, 1000) != HAL_OK);
		while(HAL_I2C_Master_Receive(&hi2c1, slaveAddress, &data[14], 6, 1000) != HAL_OK);
		temp = data[15];
		data[15] = data[14];
		data[14] = temp;
		temp = data[17];
		data[17] = data[16];
		data[16] = temp;
		temp = data[19];
		data[19] = data[18];
		data[18] = temp;
		data[20] = 0x5C;
		data[21] = 0x6E;
		CDC_Transmit_FS(data, 22);
		HAL_Delay(50);
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
		
  /* USER CODE END 3 */

   }
}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/8000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK_DIV8);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin : PA0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PD12 PD13 PD14 PD15 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void CheckSTM32wMPU9150byI2C(void)
{
	//Initializing the interaction of the STM with the MPU by I2C
	while(HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) {;} // Waiting for the bus I2C to be ready
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET); // The orange LED indicates that the bus is ready
	HAL_Delay(500); //Delay
	{
		uint8_t temporaryVariable = 0x75; // Temporary variable for sending control data
		while(HAL_I2C_Master_Transmit(&hi2c1, slaveAddress, &temporaryVariable, 1, 1000) != HAL_OK) {;}; // Correctness condition
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET); //The red LED indicates the correct initialization of the MPU9150 (Sending)
		
	}
	HAL_Delay(500); //Delay
	{
		uint8_t temporaryVariable = 0; // Temporary variable for sending control data
		while(HAL_I2C_Master_Receive(&hi2c1, slaveAddress, &temporaryVariable, 1, 1000) != HAL_OK); // Correctness condition
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET); //The blue LED indicates the correct initialization of the MPU9150 (Receiving)
		while(temporaryVariable != 0x68)
			{
				HAL_Delay(500);
				HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_15);
			}
	}
	HAL_Delay(500);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_SET);
	HAL_Delay(500);
	{
		int i = 0;
		while(i != 3)
		{
			HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_13);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_15);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);
			HAL_Delay(100);
			i = i +1 ;
		}
	}
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_SET); // The device is ready for use
	}

	
void CheckSTM32wPCbyUSB(void)
{

	while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) != 1)
	{
		HAL_Delay(50);
		HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);
		HAL_Delay(50);
	} // Waiting for a command from the user
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_RESET);
	uint8_t temporaryVariable[40] = {0x53, 0x54, 0x41, 0x52, 0x54, 0x20, 0x43, 0x4F, 0x4E, 0x54, 0x52, 0x4F, 0x4C, 0x20, 0x53,
																	 0x54, 0x4D, 0x33, 0x32, 0x46, 0x34, 0x30, 0x37, 0x56, 0x47, 0x54, 0x36, 0x20, 0x77, 0x69,
																	 0x74, 0x68, 0x20, 0x4D, 0x50, 0x55, 0x39, 0x31, 0x35, 0x30};
	uint8_t S = 0x30;
	HAL_Delay(250);
	while(CDC_Transmit_FS(&S, 1) == USBD_BUSY) {;}
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
	HAL_Delay(1000);
	while(CDC_Transmit_FS((unsigned char*)temporaryVariable, 40) != USBD_OK) {;}
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);
	HAL_Delay(250);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);
	HAL_Delay(500);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_SET);
	HAL_Delay(500);
	{
		int i = 0;
		while(i != 3)
		{
			HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_13);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_15);
			HAL_Delay(100);
			HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);
			HAL_Delay(100);
			i = i +1 ;
		}
	}
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_SET); // The device is ready for use
	while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) != 1) {;} // Waiting for a command from the user
}

void ConfigAccelMPU(void)
{
	uint8_t mpu9150_Configuration[2] = {0x6B,0x00}; // Setting the frequency 8Mhz from an internal source
	while(HAL_I2C_Master_Transmit(&hi2c1, slaveAddress, (uint8_t*)mpu9150_Configuration, 2, 1000) != HAL_OK);
	mpu9150_Configuration[0] = 0x19; // Register frequency
	mpu9150_Configuration[1] = 7; // Prescaler frequency 8 kHz/(1 + PF)
	while(HAL_I2C_Master_Transmit(&hi2c1, slaveAddress, (uint8_t*)mpu9150_Configuration, 2, 1000) != HAL_OK);
	mpu9150_Configuration[0] = 0x1C; // Register accelerometer
	while(HAL_I2C_Master_Transmit(&hi2c1, slaveAddress, (uint8_t*)mpu9150_Configuration, 1, 1000) != HAL_OK);
	uint8_t answerMPU9150 = 0;
	while(HAL_I2C_Master_Receive(&hi2c1, slaveAddress, &answerMPU9150, 1, 1000) != HAL_OK);
	answerMPU9150 = (answerMPU9150 & 0xE7) | (0x03 << 3); // Set reggister XXX03XXX +-16g
	answerMPU9150 = answerMPU9150 + 0x07;
	while(HAL_I2C_Master_Transmit(&hi2c1, slaveAddress,&answerMPU9150, 1, 1000) != HAL_OK);
	mpu9150AS = 1 / ((float)2048);
}


void ConfigGyroMPU(void)
{
	uint8_t mpu9150_Configuration = 0x1B; // Register accelerometer
	while(HAL_I2C_Master_Transmit(&hi2c1, slaveAddress, &mpu9150_Configuration, 1, 1000) != HAL_OK);
	uint8_t answerMPU9150 = 0;
	while(HAL_I2C_Master_Receive(&hi2c1, slaveAddress, &answerMPU9150, 1, 1000) != HAL_OK);
	answerMPU9150 = (answerMPU9150 & 0xE7) | (0x00 << 3); // Set reggister XXX00XXX +-2g
	answerMPU9150 = answerMPU9150 + 0x07;
	while(HAL_I2C_Master_Transmit(&hi2c1, slaveAddress, &answerMPU9150, 1, 1000) != HAL_OK);
	mpu9150GS = 1 / ((float) 131);
}

void ConfigMagnMPU(void)
{
	uint8_t mpu9150_Configuration[2] = {0x0A,0x01};  // Register magnetometr
	while(HAL_I2C_Master_Transmit(&hi2c1, slaveAddress, &mpu9150_Configuration[0], 1, 1000) != HAL_OK);
	uint8_t answerMPU9150 = 0;
	while(HAL_I2C_Master_Receive(&hi2c1, slaveAddress, &answerMPU9150, 1, 1000) != HAL_OK);
	answerMPU9150 = (answerMPU9150 & 0xF0); // Set reggister XXXX0000 
	mpu9150_Configuration[1] = answerMPU9150;
	while(HAL_I2C_Master_Transmit(&hi2c1, slaveAddress, (uint8_t*)mpu9150_Configuration, 2, 1000) != HAL_OK);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
